class CreatePrescriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :prescriptions do |t|
      t.string :name
      t.string :address
      t.string :birthday
      t.jsonb :formulation_ingredients

      t.timestamps
    end
  end
end
