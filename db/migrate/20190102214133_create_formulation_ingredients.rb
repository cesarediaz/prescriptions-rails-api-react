class CreateFormulationIngredients < ActiveRecord::Migration[5.2]
  def change
    create_table :formulation_ingredients do |t|
      t.references :formulation
      t.references :ingredient
      t.float :percentage

      t.timestamps
    end
  end
end
