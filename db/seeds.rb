require 'csv'

formulations = File.read(Rails.root.join('db', 'seeds', 'formulations.csv'))
csv_formulations = CSV.parse(formulations, headers: true, encoding: 'ISO-8859-1')

csv_formulations.each do |row|
  Formulation.create! row.to_hash
end

ingredients = File.read(Rails.root.join('db', 'seeds', 'ingredients.csv'))
csv_ingredients = CSV.parse(ingredients, headers: true, encoding: 'ISO-8859-1')

csv_ingredients.each do |row|
  Ingredient.create! row.to_hash
end


formulation_ingredients = File.read(Rails.root.join('db', 'seeds', 'formulation_ingredients.csv'))
csv_formulation_ingredients = CSV.parse(formulation_ingredients, headers: true, encoding: 'ISO-8859-1')

csv_formulation_ingredients.each do |row|
  FormulationIngredient.create! row.to_hash
end
