require 'test_helper'

class FormulationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get formulations_index_url
    assert_response :success
  end

  test "should get create" do
    get formulations_create_url
    assert_response :success
  end

  test "should get update" do
    get formulations_update_url
    assert_response :success
  end

  test "should get destroy" do
    get formulations_destroy_url
    assert_response :success
  end

end
