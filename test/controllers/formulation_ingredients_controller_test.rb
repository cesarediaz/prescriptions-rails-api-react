require 'test_helper'

class FormulationIngredientsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get formulation_ingredients_index_url
    assert_response :success
  end

  test "should get create" do
    get formulation_ingredients_create_url
    assert_response :success
  end

  test "should get update" do
    get formulation_ingredients_update_url
    assert_response :success
  end

  test "should get destroy" do
    get formulation_ingredients_destroy_url
    assert_response :success
  end

end
