import {  GET_INGREDIENTS_OR_FORMULATIONS
        } from '../actions/actionTypes';

function ingredientsReducer(state = [], action) 
{
  switch(action.type) {
    case GET_INGREDIENTS_OR_FORMULATIONS:
      return action.ingredients;
    default:
      return state;
  }
}

export default ingredientsReducer;
