import { combineReducers } from 'redux'
import prescriptionsReducer from './prescriptionsReducer'
import ingredientsReducer from './ingredientsReducer'

const rootReducer = combineReducers({
  prescriptions: prescriptionsReducer,
  ingredients: ingredientsReducer,
});

export default rootReducer;
