import {  GET_PRESCRIPTIONS,
          ADD_PRESCRIPTION,
          REMOVE_PRESCRIPTION
        } from '../actions/actionTypes';

function prescriptionsReducer(state = [], action) 
{
  switch(action.type) {
    case GET_PRESCRIPTIONS:
      return action.prescriptions;
    case ADD_PRESCRIPTION:
      return [
          ...state,
          action.prescription
      ];
    case REMOVE_PRESCRIPTION:
      return state.filter(({ id }) => parseInt(id) !== parseInt(action.id));
    default:
      return state;
  }
}

export default prescriptionsReducer;
