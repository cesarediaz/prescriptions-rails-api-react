import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NotFoundPage from '../components/NotFoundPage';
import DashboardPage from '../components/DashboarPage';
import AddPrescriptionPage from '../components/AddPrescriptionPage';
import EditPrescriptionPage from '../components/EditPrescriptionPage';
import Pdf from '../components/Pdf';
import Header from '../components/Header';

const AppRouter = () => (
  <Router>
    <div>
      <Header />
      <Switch>
        <Route path='/' component={DashboardPage} exact={true} />
        <Route path='/prescriptions/new' component={AddPrescriptionPage} />
        <Route path='/prescriptions/:id/edit' component={EditPrescriptionPage} />
        <Route path="/prescriptions/:id" component={Pdf} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;
