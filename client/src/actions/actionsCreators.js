import {  ADD_PRESCRIPTION,
          EDIT_PRESCRIPTION,
          GET_PRESCRIPTION,
          GET_PRESCRIPTIONS,
          REMOVE_PRESCRIPTION,
          GET_INGREDIENTS_OR_FORMULATIONS
        } from '../actions/actionTypes';
import axios from 'axios';

// REMOVE PRESCRIPTION
export const removePrescription = (id) => ({
  type: REMOVE_PRESCRIPTION,
  id
});

// START_REMOVE_PRESCRIPTION
export const startRemovePrescription = (id) => {
  return (dispatch) => {
    return axios.delete(`/api/v1/prescriptions/${id}`)
    .then(response => {
      dispatch(removePrescription(id));
    })
    .catch(error => console.log(error))
  }
}

// GET_INGREDIENTS_OR_FORMULATIONS
export const getIngredientsOrFormulations = (ingredients) => ({
  type: GET_INGREDIENTS_OR_FORMULATIONS,
  ingredients
});

// START_GET_INGREDIENTS_OR_FORMULATIONS
export const startGetIngredientsOrFormulations = (q) => {
  return (dispatch) => {
    return axios.get('/api/v1/ingredients/search', {
      params: {
        q: q
      }
    })
    .then(response => {
      dispatch(getIngredientsOrFormulations(response.data));
    })
    .catch(error => console.log(error))
  }
}

// ADD_PRESCRIPTION
export const addPrescription = (prescription) => ({
  type: ADD_PRESCRIPTION,
  prescription
})

// START_ADD_PRESCRIPTION
export const startAddPrescription = (name, address, birthday, formulation_ingredients) => {
  const prescription = { 
    name: name,
    address: address,
    birthday: birthday,
    formulation_ingredients: formulation_ingredients
  };

  return (dispatch) => {
    return axios.post('/api/v1/prescriptions', prescription)
    .then(response => {
      dispatch(addPrescription({id: response.data.id, ...prescription}));
    })
    .catch(error => console.log(error))
  }
}

// EDIT_PRESCRIPTION
export const editPrescription = (data) => ({
  type: EDIT_PRESCRIPTION,
  id: data.id,
  prescription: data.prescription
})

// START_EDIT_PRESCRIPTION
export const startEditPrescription = (id, updates) => {
  const prescription = {
    prescription: {
      id: id,
      name: updates.name,
      address: updates.address,
      birthday: updates.birthday,
      formulation_ingredients: updates.formulation_ingredients
    }
  };

  return (dispatch) => {
    return axios.put(`/api/v1/prescriptions/${id}`, prescription)
      .then(response => {
        dispatch(editPrescription(response))
      })
      .catch(error => console.log(error))
    };
};



// GET_PRESCRIPTION
export const getPrescription = (prescription) => ({
  type: GET_PRESCRIPTION,
  prescription
})

// START_PRINT_PRESCRIPTION
export const startPrintPrescription = (id) => {
  return (dispatch) => {
    return axios.get(`/api/v1/prescriptions/${id}`)
      .then(response => {
        dispatch(getPrescription(response.data))
      })
      .catch(error => console.log(error))
    };
};

// GET_PRESCRIPTIONS
export const getPrescriptions = (prescriptions) => {
  return {
    type: GET_PRESCRIPTIONS,
    prescriptions
  }
};

// SET_GET_PRESCRIPTIONS
export const startGetPrescriptions = () => {
  let prescriptions = [];
  return (dispatch) => {
    return axios.get('/api/v1/prescriptions')
    .then(response => {
        response.data.forEach((prescription) => {
          prescriptions.push(prescription);
        });
        dispatch(getPrescriptions(prescriptions));
      }
    )
  };
};
