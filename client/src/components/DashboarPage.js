import React, { Component } from 'react';
import { connect } from 'react-redux';
import { startGetPrescriptions, startRemovePrescription } from '../actions/actionsCreators';
import { Link } from 'react-router-dom';

class DashboardPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      prescriptions: props.prescriptions ? props.prescriptions : []
    }
  }

  onRemove = (e) => {
    this.props.startRemovePrescription(e.target.id);
  }

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Address</th>
              <th scope="col">Ingredients</th>
              <th>Print</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {this.props.prescriptions.map((prescription) => {
              return(
                <tr key={prescription.id}>
                  <td>{prescription.id}</td>
                  <td>{prescription.name}</td>
                  <td>{prescription.address}</td>
                  <td>
                    {prescription.formulation_ingredients.map((m) => {
                      return <div key={m.id}>
                              <p>{m.name} <small>({m.percentage})</small></p>
                            </div>
                    })}
                  </td>

                  <td>
                    <Link to={`/prescriptions/${prescription.id}`}
                          className="btn btn-outline-secondary btn-sm">
                      Pdf
                    </Link>
                  </td>
                  <td>
                    <Link to={`/prescriptions/${prescription.id}/edit`}
                          className="btn btn-outline-warning btn-sm">
                      Edit
                    </Link>
                  </td>
                  <td>
                    <button id={prescription.id}
                            onClick={(e) => { if (window.confirm('Are you sure you wish to delete this prescription?')) this.onRemove(e) } }
                            className="btn btn-outline-danger btn-sm">
                      Delete
                    </button>
                  </td>
                </tr>
                )
              })
            }
            </tbody>
          </table>
        </div>
      )
    }
}

const mapStateToProps = (state) => ({
  prescriptions: state.prescriptions
});

const mapDispatchToProps = (dispatch) => ({
  startGetPrescriptions: dispatch(startGetPrescriptions()),
  startRemovePrescription: (id) => dispatch(startRemovePrescription(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);
