import React from 'react';
import $ from 'jquery';

class NewIngredient extends React.Component {
  handlePercentageChange = (e) => {
    let new_ingredients = [...this.props.new_ingredients];
    new_ingredients[e.target.dataset.id]['percentage'] = e.target.value;
    this.props.handlerNewIngredients(new_ingredients);
  }

  removeItem = (e) => {
    $(`#${e.target.id}`).parent('div').parent('div').remove()
    let id = e.target.id.split('-')[1]
    this.props.handlerRemoveIngredient(id)
  }

  render() {
    return (
      <div>
        {
          this.props.new_ingredients.length === 0 ? ( 
            console.log('No New Ingredients')
          ) : (
            this.props.new_ingredients.map((new_ingredient, idx) => {
                return  <div key={new_ingredient.id}>
                          <div className="row bg-light new-ingredient">
                            <div className="col-sm">
                              <hr />
                              <h5 className="card-title">{new_ingredient.name}</h5>
                              <p className="card-text">{new_ingredient.description}</p>
                            </div>
                            <div className="col-sm">
                              <hr />

                              <input  className="form-control"
                                      type="number"
                                      step="0.01"
                                      data-id={idx}
                                      value={new_ingredient.percentage ? new_ingredient.percentage : new_ingredient.minimum_percentage}
                                      min={new_ingredient.minimum_percentage}
                                      max={new_ingredient.maximum_percentage}
                                      name="percentage"
                                      onChange={this.handlePercentageChange}
                                      >
                              </input>
                              <small>
                                Minimum Percentage : {new_ingredient.minimum_percentage}
                                -
                                Maximum Percentage : {new_ingredient.maximum_percentage}
                              </small>
                                <div id={`item-${new_ingredient.id}`}
                                    onClick={this.removeItem}
                                    className="btn btn-outline-danger btn-sm float-right">
                                  Remove
                                </div>
                              </div>
                            </div>
                          </div>
              })
            )
        }
      </div>
    )
  }
}

export default NewIngredient;
