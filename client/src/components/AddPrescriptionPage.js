import React, { Component } from 'react';
import { connect } from 'react-redux';
import { startAddPrescription, startGetIngredientsOrFormulations } from '../actions/actionsCreators';
import PrescriptionForm from "./PrescriptionForm";

class AddPrescriptionPage extends Component {
  componentDidMount() {
    this.props.startGetIngredientsOrFormulations();
  }

  render() {
    return (
      <div>
        <PrescriptionForm history={this.props.history}
                          startAddPrescription={this.props.startAddPrescription}
                          startGetIngredientsOrFormulations={this.props.startGetIngredientsOrFormulations}
                          action='create'
        />
      </div>
    )
  }
}

const mapStateToProps = function(state){
  return {
    ingredients: state.ingredients
  }
}

const mapDispatchToProps = (dispatch) => ({
  startAddPrescription: (name, address, birthday, formulation_ingredients) => dispatch(startAddPrescription(name, address, birthday, formulation_ingredients)),
  startGetIngredientsOrFormulations: (q) => dispatch(startGetIngredientsOrFormulations(q))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddPrescriptionPage);

