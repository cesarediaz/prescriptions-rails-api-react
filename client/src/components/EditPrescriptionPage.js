import React, { Component } from 'react';
import { connect } from 'react-redux';
import { startEditPrescription, startGetIngredientsOrFormulations } from '../actions/actionsCreators';
import PrescriptionForm from "./PrescriptionForm";

class EditPrescriptionPage extends Component {
  componentDidMount() {
    this.props.startGetIngredientsOrFormulations();
  }

  render() {
    return (
      <div>
        <PrescriptionForm history={this.props.history}
                          startEditPrescription={this.props.startEditPrescription}
                          startGetIngredientsOrFormulations={this.props.startGetIngredientsOrFormulations}
                          prescription={this.props.prescription}
                          action='update'
                          id={this.props.prescription.id}
        />
      </div>
    )
  }
}

const mapStateToProps = (state, props) => ({
  prescription: state.prescriptions.find((prescription) => parseInt(prescription.id) === parseInt(props.match.params.id))
})

const mapDispatchToProps = (dispatch) => ({
  startEditPrescription: (id, {name, address, birthday, formulation_ingredients}) => dispatch(startEditPrescription(id, {name, address, birthday, formulation_ingredients})),
  startGetIngredientsOrFormulations: (q) => dispatch(startGetIngredientsOrFormulations(q))
})
  
export default connect(mapStateToProps, mapDispatchToProps)(EditPrescriptionPage);
  