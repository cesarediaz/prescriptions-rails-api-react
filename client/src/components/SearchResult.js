import React from 'react';
import axios from 'axios';
import NewIngredient from './NewIngredient';
import { connect } from 'react-redux';

class SearchResult extends React.Component {
  constructor(props) {
    super(props);
    this.handlerNewIngredients = this.handlerNewIngredients.bind(this);
    this.handlerRemoveIngredient = this.handlerRemoveIngredient.bind(this);
    this.state = {
      new_ingredients: this.props.new_ingredients ? this.props.new_ingredients : [],
      error: ''
    };
  }

  handlerNewIngredients(added_ingredients) {
    this.setState({added_ingredients})
  };

  handlerRemoveIngredient(id) {
    this.props.handlerRemoveIngredient(id)
    this.setState({new_ingredients: this.state.new_ingredients.filter((ingredient) => ( parseInt(ingredient.id) !== parseInt(id) ))});
  };


  addNewIngredient = (ingredient, ingredients) => {
    if (ingredients.filter(e => e.name === ingredient.name).length > 0) {
      const error = `Ingredient *${ingredient.name}* has been already added`;

      this.setState(() => ( { error } ));
    
    } else {

      Object.assign(ingredient, {percentage: ingredient.percentage});

      this.setState((prevState) => ({new_ingredients: [...prevState.new_ingredients, ingredient]}));

      this.props.handlerNewIngredients(this.state.new_ingredients);

      this.setState(() => ( { error: '' } ));
    }
    document.getElementById('formulation_ingredients').value = null;
    this.props.handlerIsSearchResultsVisible(false);
  }

  handleFormulationClick = (e) => {
      axios.get(`/api/v1/formulations/${e.target.id}/formulation`)
      .then(response => {
          let i = 0;
          for (i = 0; i < response.data.ingredients.length; i++) {
            this.addNewIngredient(response.data.ingredients[i], this.state.new_ingredients);
          }
      })
      .catch(error => console.log(error))
  };

  handleSingleIngredientClick = (e) => {
    if (e.target.id) {
      axios.get(`/api/v1/ingredients/${e.target.id}/ingredient`)
      .then(response => {
        this.addNewIngredient(response.data, this.state.new_ingredients);
      })
      .catch(error => console.log(error))
    }
  };

  render() {
    return (
      <div id='ingredientsList'>
        {this.state.error && 
          <div className="alert alert-danger" role="alert">
            {this.state.error}
          </div>
          
        }
        {
          this.props.ingredients.length === 0 || !this.props.isSearchResultsVisible ? ( 
            console.log('No hay coincidencias!')
          ) : 
          
          (
            this.props.ingredients.filter(item => (item.name.indexOf(this.props.searchText) > -1 || (item.description && item.description.indexOf(this.props.searchText) > -1) )).map((ingredient) => {
              const key = ingredient.id + Math.random();
              return <div className="row" key={key}>
              <div className="col-sm-12 form-group">
                <div className="card">
                  <div
                    id={ingredient.id}
                    onClick={ingredient.ingredients ? this.handleFormulationClick : this.handleSingleIngredientClick}
                    className="btn btn-primary">
                    Add
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">{ingredient.name}</h5>
                    <p className="card-text">{ingredient.description}</p>
                  </div>
                  {ingredient.ingredients ? ingredient.ingredients.map((i) => <div key={Math.random()}>{i.name}</div>) : '' }
                </div>    
              </div>
            </div>
            })
          )              
        }
        <NewIngredient  new_ingredients={this.state.new_ingredients}
                        handlerNewIngredients={this.handlerNewIngredients}
                        handlerRemoveIngredient={this.handlerRemoveIngredient}
                        />
      </div>
    )
  }
}
const mapStateToProps = function(state){
  return {
    ingredients: state.ingredients
  }
}

export default connect(mapStateToProps)(SearchResult);
