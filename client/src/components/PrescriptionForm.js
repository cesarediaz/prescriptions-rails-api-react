import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import SearchResult from "./SearchResult";
import moment from 'moment';
import $ from 'jquery';
import { RULES } from './rules/validation';

export default class PrescriptionForm extends Component {
  constructor(props) {
    super(props);
    this.handlerNewIngredients = this.handlerNewIngredients.bind(this);
    this.handlerIsSearchResultsVisible = this.handlerIsSearchResultsVisible.bind(this);

    this.state = {
      id: props.id ? props.id : undefined,
      name: props.prescription ? props.prescription.name : '',
      address: props.prescription ? props.prescription.address : '',
      startDate: props.prescription ? new Date(moment(props.prescription.birthday, 'MM/DD/YYYY')) : new Date(moment().subtract(1, "days")),
      formErrors: {name: '', address:''},
      formValidity: {name: false, address: false},

      ingredients: props.prescription ? props.prescription.formulation_ingredients : [],
      added_ingredients: props.prescription ? props.prescription.formulation_ingredients : [],
      isSearchResultsVisible: false,
      searchText: '',
      isSubmitDisabled: true,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFieldsChange = this.handleFieldsChange.bind(this);
    this.handlerRemoveIngredient = this.handlerRemoveIngredient.bind(this);
  }

  componentDidMount() {
    const isSubmitDisabled = this.state.ingredients.length > 0 ? false : true;
    this.setState(() => ( { isSubmitDisabled } ));

    this.validateField('name', this.state.name);
    this.validateField('address', this.state.address);
  }

  handleFieldsChange(e){
    let  name = e.target.name
    let  value = e.target.value
    this.setState({
      [name]: value,
    }, function(){ this.validateField(name, value)})
  }

  handlerNewIngredients(added_ingredients) {
    this.setState({ added_ingredients })
    this.canSubmit()
  }

  handlerIsSearchResultsVisible(value) {
    this.setState({ isSearchResultsVisible: value })
  }

  handlerRemoveIngredient = (id) => {
    this.setState({added_ingredients: this.state.added_ingredients.filter((ingredient) => ( parseInt(ingredient.id) !== parseInt(id) ))});
  }

  handleChange(date) {
    this.setState({startDate: date});
  }

  validateField(name, value) {
    const fieldValidationErrors = this.state.formErrors
    const validity = this.state.formValidity
    const rule = RULES.fields[`${name}`];
  
    validity[name] = value && value.length >= rule.minimum_length

    fieldValidationErrors[name] = validity[name] ? '': `"${name}" ${rule.message} : ${rule.minimum_length}`;
    this.setState({
      formErrors: fieldValidationErrors,
      formValidity: validity,
    }, () => this.canSubmit())
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'is-invalid');
  }

  canSubmit() {
    let valid = this.state.formValidity.name && this.state.formValidity.address && this.state.added_ingredients.length > 0 ? false : true;
    this.setState({ isSubmitDisabled: valid });
  }

  search = (e) => {
    const isSearchResultsVisible = e.target.value.length > 0 ? true : false;
    this.setState(() => ( { isSearchResultsVisible } ));

    let searchText = e.target.value;
    this.setState(() => ( { searchText } ));
  }

  onSubmit = (e) => {
    e.preventDefault();
    let name = $('#name').val();
    let address = $('#address').val();
    let birthday = $('#birthday').val();

    let formulation_ingredients = this.state.added_ingredients;
    let id = this.state.id;

    if (id === undefined) {
      this.props.startAddPrescription(name, address, birthday, formulation_ingredients)
    } else {
      this.props.startEditPrescription(id, {name, address, birthday, formulation_ingredients})
    }

    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <div className="container">
          {this.state.error && 
            <div className="alert alert-danger" role="alert">
              {this.state.error}
            </div>
          }
          <div className="row">
            <div className="col-md-12 col-md-offset-3">
              <form onSubmit={this.onSubmit} action={this.props.action} id={this.props.id}>
                <div className="row">
                  <label htmlFor="patientName" className="col-sm-6 col-form-label">Patient Name<small>(*)</small></label>
                  <div className="col-sm-12 form-group">
                    <input  type="text"
                            name="name"
                            placeholder="Name"
                            maxLength="50"
                            id="name"
                            onChange={this.handleFieldsChange}
                            value={this.state.name}
                            className={`form-control ${this.errorClass(this.state.formErrors.name)}`}
                            />
                     <div className="invalid-feedback">{this.state.formErrors.name}</div>
                  </div>
                </div>

                <div className="row">
                  <label htmlFor="address" className="col-sm-6 col-form-label">Address<small>(*)</small></label>
                  <div className="col-sm-12 form-group">
                    <input  type="text"
                            name="address"
                            placeholder="Address"
                            maxLength="50"
                            id="address"
                            className={`form-control ${this.errorClass(this.state.formErrors.address)}`}
                            value={this.state.address}
                            onChange={this.handleFieldsChange}
                            />
                    <div className="invalid-feedback">{this.state.formErrors.address}</div>
                  </div>
                </div>

                <div className="row">
                  <label htmlFor="birthday" className="col-sm-6 col-form-label">Birthday</label>
                  <div className="col-sm-12 form-group">
                    <DatePicker
                      id="birthday"
                      name="birthday"
                      selected={this.state.startDate}
                      onChange={this.handleChange}
                      value={this.state.birthday}
                      filterDate={(date) => {
                        return moment().subtract(100, "years") < date && moment().subtract(1, "days") > date;
                      }}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 form-group">
                  <label htmlFor="IngredientOrFormulation">Ingredient or Formulation<small>(*)</small></label>
                  
                  <input  type="text"
                          placeholder="Type an ingredient of formulation"
                          maxLength="50"
                          id="formulation_ingredients"
                          className="form-control"
                          onChange={this.search}
                           />
                  </div>
                  <SearchResult searchText={this.state.searchText}
                                isSearchResultsVisible={this.state.isSearchResultsVisible}
                                handlerIsSearchResultsVisible={this.handlerIsSearchResultsVisible}
                                handlerIngredients={this.handlerIngredients}
                                handlerNewIngredients={this.handlerNewIngredients}
                                handlerRemoveIngredient={this.handlerRemoveIngredient}
                                new_ingredients={this.state.ingredients}
                                />
                </div>

                <div className="row">
                  <div className="col-sm- form-group">
                    <button className="btn btn-secondary btn-block"
                            disabled={this.state.isSubmitDisabled}>
                            Submit
                    </button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
