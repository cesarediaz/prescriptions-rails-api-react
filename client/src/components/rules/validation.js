const NOT_EMPTY = 'should not be empty';
const MINIMUM_LENGTH = 'should have a minimum length of characters';

export const RULES = {
  fields: {
    name: {
      message: `${NOT_EMPTY} and ${MINIMUM_LENGTH}`,
      minimum_length: 10
    },
    address: {
      message: `${NOT_EMPTY} and ${MINIMUM_LENGTH}`,
      minimum_length: 20
    }
  } 
};
