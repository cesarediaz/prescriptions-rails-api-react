import React, { Component } from 'react';
import { connect } from 'react-redux';
import PDF, { Text, Table } from 'jspdf-react';
import { startPrintPrescription } from '../actions/actionsCreators';

class Pdf extends Component {
  constructor(props) {
    super(props)
    this.state = {
      prescription: props.prescription ? props.prescription : '',
    }
  }

  render () {
    const properties = {title: 'Prescriptions'};
    const columns = [ "Name", "% (percentage)" ];
    const rows = [];
    if (this.state.prescription.formulation_ingredients && this.state.prescription.formulation_ingredients.length > 0) {
      this.state.prescription.formulation_ingredients.map((ingredient) => {
        return rows.push([ingredient.name, ingredient.percentage])
      })
    }

    return (
      rows.length > 0 ? (<React.Fragment>
        <PDF
          properties={ properties }
          preview={ true }
        >
          <Text x={35} y={25} size={10}>{`Prescriptions for patient`}</Text>
          <Text x={35} y={35} size={12}>
            {`Name: ${this.state.prescription.name}, Address ${this.state.prescription.address}, Birthday: ${this.state.prescription.birthday}`}
          </Text>
          <Table columns={columns} rows={rows} />
        </PDF>
      </React.Fragment>) : null
    )
  }
}

const mapStateToProps = (state, props) => ({
  prescription: state.prescriptions.find((prescription) => parseInt(prescription.id, 10) === parseInt(props.match.params.id))
})

const mapDispatchToProps = (dispatch) => ({
  printPrescription: (id) => dispatch(startPrintPrescription(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Pdf);
