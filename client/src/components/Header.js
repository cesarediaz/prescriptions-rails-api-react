import React from 'react';
import { NavLink } from 'react-router-dom'
import { Navbar  } from 'react-bootstrap';
const Header = () => (
  <Navbar>
    <NavLink  to='/'
              exact={true}
              className='btn btn-sm btn-outline-primary'>
              Dashboard
    </NavLink>

    <NavLink  to='/prescriptions/new'
              className='btn btn-sm btn-outline-primary'>
              Create Prescription
    </NavLink>
  </Navbar>
);

export default Header;
