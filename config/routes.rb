Rails.application.routes.draw do
  scope '/api/v1' do
    resources :formulations do
      collection do
        get 'search'
      end
      member do
        get 'formulation'
      end
    end
    resources :ingredients do
      collection do
        get 'search'
      end
      member do
        get 'ingredient'
      end
    end
    resources :formulation_ingredients
    resources :prescriptions
  end
end
