class Formulation < ApplicationRecord
  has_many :formulation_ingredient
  has_many :ingredients, through: :formulation_ingredient
end
