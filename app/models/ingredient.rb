class Ingredient < ApplicationRecord
  has_many :formulation_ingredient
  has_many :formulations, through: :formulation_ingredient
end
