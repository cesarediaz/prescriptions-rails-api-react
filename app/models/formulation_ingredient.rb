class FormulationIngredient < ApplicationRecord
  belongs_to :ingredient
  belongs_to :formulation
end
