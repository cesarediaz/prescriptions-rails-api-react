class PrescriptionsController < ApplicationController
  def index
    rescriptions = Prescription.order('created_at DESC')
    render json: rescriptions
  end

  def show
    prescription = Prescription.find(params[:id])
    render json: prescription
  end

  def create
    prescription = Prescription.create(prescription_param)
    render json: prescription
  end

  def update
    prescription = Prescription.find(params[:id])
    prescription.update_attributes(prescription_param)
    render json: prescription
  end

  def destroy
    prescription = Prescription.find(params[:id])
    prescription.destroy
    head :no_content, status: :ok
  end

  private

  def prescription_param
    params
      .require(:prescription)
      .permit(:name,
              :address,
              :birthday,
              formulation_ingredients: [:id,
                                        :name,
                                        :minimum_percentage,
                                        :maximum_percentage,
                                        :description,
                                        :percentage,
                                        :classes,
                                        :created_at,
                                        :updated_at]
            )
  end

end
