class FormulationsController < ApplicationController
  def index
    formulations = Formulation.order('created_at DESC')
    render json: formulations
  end

  def create
    ingredient = Formulation.create(ingredient_param)
    render json: ingredient
  end

  def update
    ingredient = Formulation.find(params[:id])
    ingredient.update_attributes(ingredient_param)
    render json: ingredient
  end

  def destroy
    ingredient = Formulation.find(params[:id])
    ingredient.destroy
    head :no_content, status: :ok
  end

  def formulation
    formulation = Formulation.find(params[:id])
    render json: formulation.as_json(include: :ingredients)
  end

  private

  def ingredient_param
    params
      .require(:ingredient)
      .permit(:name)
  end
end
