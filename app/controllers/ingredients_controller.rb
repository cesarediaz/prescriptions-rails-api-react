class IngredientsController < ApplicationController
  def index
    ingredients = Ingredient.order('created_at DESC')
    render json: ingredients
  end

  def create
    ingredient = Ingredient.create(ingredient_param)
    render json: ingredient
  end

  def update
    ingredient = Ingredient.find(params[:id])
    ingredient.update_attributes(ingredient_param)
    render json: ingredient
  end

  def destroy
    ingredient = Ingredient.find(params[:id])
    ingredient.destroy
    head :no_content, status: :ok
  end

  def search
    ingredients = Ingredient.all
    formulations = Formulation.all
    result = ingredients.as_json + formulations.as_json(include: :ingredients)
    render json: result
  end

  def ingredient
    ingredient = Ingredient.find(params[:id])
    render json: ingredient.as_json(only: [:id,
                                           :name,
                                           :description,
                                           :minimum_percentage,
                                           :maximum_percentage])
  end
  private

  def ingredient_param
    params
      .require(:ingredient)
      .permit(:name,
              :minimum_percentage,
              :maximum_percentage,
              :description,
              :classes)
  end
end
